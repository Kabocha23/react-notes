import React, { Component } from 'react';
import EditForm from '../EditForm/EditForm';
import './Note.css';
import PropTypes from 'prop-types';

class Note extends Component {

    constructor(props){
        super(props);
        this.noteContent = props.noteContent;
        this.noteId = props.noteId;
        this.handleRemoveNote = this.handleRemoveNote.bind(this);
        this.handleEditNote = this.handleEditNote.bind(this);
        this.handleToggleEdit = this.handleToggleEdit.bind(this);

    }

    handleRemoveNote(id){
        this.props.removeNote(id);
    }

    handleEditNote(id, updatedNoteContent){
        this.props.editNote(id, updatedNoteContent)
        console.log(`Updated from Note: ${updatedNoteContent}`)
    }

    handleToggleEdit(id){
        this.props.toggleEditmode(id);
    }

    render(props){
        return (
            <div className='note fade-in '>

                <span 
                    className='closebtn' 
                    onClick={() => this.handleRemoveNote(this.noteId)}
                >
                    &times;
                </span>

                <span
                    className='editbtn'
                    onClick={() => this.handleToggleEdit(this.noteId)}
                >
                    Edit
                </span>
                <div>
                    {
                        this.props.editable == this.props.noteId ?
                        
                        <EditForm
                            noteId={this.props.noteId}
                            noteContent={this.props.noteContent}
                            handleEditNote={this.handleEditNote}
                            updatedNotecontent={this.props.updatedNotecontent}
                        /> :
                        <p className='noteContent'>{ this.noteContent }</p>
                    }
                </div>
            </div>
        )
    }
}

Note.propTypes = {
    noteContent: PropTypes.string
}

export default Note;