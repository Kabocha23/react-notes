import React, { Component } from 'react';
import Note from './Note/Note';
import NoteForm from './NoteForm/NoteForm';
import { DB_CONFIG } from './config/config';
import firebase from 'firebase/app';
import 'firebase/database';
import './App.css';

class App extends Component {

  constructor(props){
    super(props);
    this.addNote = this.addNote.bind(this);
    this.removeNote = this.removeNote.bind(this);
    this.editNote = this.editNote.bind(this);
    this.toggleEditmode = this.toggleEditmode.bind(this);

    this.app = firebase.initializeApp(DB_CONFIG);
    this.db = this.app.database().ref().child('notes');

    this.state = {
      notes: [],
      editable: '',
      updatedNotecontent: '',
    }
    
  }

  componentWillMount(){
    console.log(`State from componentWillMount: ${JSON.stringify(this.state)}`);
    const previousNotes = this.state.notes;

    this.db.on('child_added', snap => {
      previousNotes.push({
        id: snap.key,
        noteContent: snap.val().noteContent,
      })

      this.setState({
        notes: previousNotes
      })
    })

    this.db.on('child_removed', snap => {
      for(let i = 0; i < previousNotes.length; i++){
        if(previousNotes[i].id === snap.key){
          previousNotes.splice(i, 1);
        }
      }

      this.setState({
        notes: previousNotes
      })
    })

    this.db.on('child_changed', snap =>{
      // const noteChanged = document.getElementById(snap.key);
      // noteChanged.innerText = snap.val();
      for(let i = 0; i < previousNotes.length; i++){
        if(previousNotes[i].id === snap.key){
          previousNotes[i] = {
            noteContent: snap.val().noteContent
          }
        }
      }

      this.setState({
        notes: previousNotes
      })
    })

  }

  addNote(note){
    this.db.push().set({ noteContent: note });
    console.log("This is the noteContent added: " + note)
  }

  removeNote(noteId){
    console.log("removeNote is receiving: " + noteId);
    this.db.child(noteId).remove()
  }

  editNote(noteId, updatedNote){
    console.log("editNote id: " + noteId);
    console.log("editNote note: " + updatedNote);
    this.db.child(noteId).update({ noteContent: updatedNote });

    this.setState({
      editable: '',
    });
  }

  toggleEditmode(noteId){
    console.log(`This is the noteId from App.js: ${noteId}`)
    console.log(this.state.notes)
    if(this.state.editable === noteId){
      this.setState({
        editable: ''
      });
    } else {
      this.setState({
        editable: noteId
      });
    }
  }


  render() {
    return (
      <div className='notesWrapper'>

        <div className='notesHeader'>
          <div className='heading'>React & Firebase Notes App</div>
        </div>

        <div className='notesInput'>
          <NoteForm 
            addNote={this.addNote}
          />
        </div>

        <div className='notesBody'>

        {/* React needs a stable unique identifier for each item in the array (which is the key). This key will be the record id from Firebase. */}
          {
            this.state.notes.map((note) => {
              return (
                <Note 
                  noteContent={note.noteContent} 
                  noteId={note.id} 
                  key={note.id} 
                  removeNote={this.removeNote}
                  editNote={this.editNote}
                  toggleEditmode={this.toggleEditmode}
                  editable={this.state.editable}
                  updatedNotecontent={this.state.updatedNotecontent}
                />
              )
            })
          }
        </div>

      </div>

    );
  }
}

export default App;
