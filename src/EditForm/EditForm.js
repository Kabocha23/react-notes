import React, { Component } from 'react';
import './EditForm.css';

class EditForm extends Component {
    constructor(props){
        super(props);
        this.handleUserInput = this.handleUserInput.bind(this);
        this._handleKeyPress = this._handleKeyPress.bind(this);
        this.handleSubEdit = this.handleSubEdit.bind(this);

        this.state = {
            updatedNoteContent: `${this.props.noteContent}`
        };
    }

    componentDidUpdate(){
        console.log(`updatedNoteContent from EditForm: ${this.state.updatedNoteContent}`);
    }

    handleUserInput(e){
        this.setState({
            updatedNoteContent: e.target.value, // value of input
          });
    }

    _handleKeyPress(e){
        if (e.key === 'Enter') {
          this.handleSubEdit();
        }
    }

    handleSubEdit(){
        this.props.handleEditNote(this.props.noteId, this.state.updatedNoteContent);

        this.setState({
            updatedNoteContent: '',
        })
    }

    render(props){
        return(
            <div className='editFormWrapper'>
                    <input 
                        className='editNoteInput'  
                        defaultValue={this.props.noteContent}
                        onKeyPress={this._handleKeyPress} 
                        onChange={this.handleUserInput} 
                    />
                    <button 
                        className='submitEditBtn' 
                        onChange={this.handleUserInput}
                        onClick={this.handleSubEdit}
                    >
                        Edit
                    </button>
            </div>
        )
    }
}

export default EditForm;