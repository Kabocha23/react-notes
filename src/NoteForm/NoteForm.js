import React, { Component } from 'react';
import './NoteForm.css';

class NoteForm extends Component {
    constructor(props){
        super(props);
        this.handleUserInput = this.handleUserInput.bind(this);
        this.writeNote = this.writeNote.bind(this);

        this.state = {
            newNoteContent: ''
        };

    }


    // When the user input changes, set newNoteContent to be the value of the input
    handleUserInput(e){
        this.setState({
            newNoteContent: e.target.value, // value of input
        })
    }

    writeNote(){
        this.props.addNote(this.state.newNoteContent)

        // Set newNoteContent to empty string after submit
        this.setState({
            newNoteContent: ''
        });
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            console.log("Enter key pressed")
          this.writeNote();
        }
    }

    render(){
        return(
                <div className='formWrapper'>
                    <input 
                        className='noteInput' 
                        placeholder='Write a New Note' 
                        value={this.state.newNoteContent} 
                        onKeyPress={this._handleKeyPress} 
                        onChange={this.handleUserInput} 
                    />
                    <button className='noteButton' onClick={this.writeNote}>Add Note</button>
                </div>
                
        )
    }
}

export default NoteForm;